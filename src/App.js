import "./App.css";
import Display from "./components/Display";
import Counter from "./components/Counter";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>Faça a alteração dos valores!!</h3>
        <Display />
        <Counter />
      </header>
    </div>
  );
}

export default App;
