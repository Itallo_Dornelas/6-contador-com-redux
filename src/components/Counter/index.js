import { useDispatch } from "react-redux";
import { addNumber, subNumber } from "../../store/modules/Counter/actions";

const Counter = () => {
  const dispatch = useDispatch();

  return (
    <>
      <button onClick={() => dispatch(addNumber(1))}>Somar +</button>
      <button onClick={() => dispatch(subNumber(1))}>Subtrair -</button>
    </>
  );
};

export default Counter;
